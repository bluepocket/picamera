#!/bin/bash

v4l2-ctl --device=/dev/video0 --set-fmt-video=width=1920,height=1080,pixelformat=1 && fswebcam -d /dev/video0 -r 1920x1080 -S 5 -D 1 --jpeg 95 "$1/snapshot-`date +"%Y-%m-%d_%H.%M.%S"`.jpg" 

