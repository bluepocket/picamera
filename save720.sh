#! /bin/bash
FPS=30
WIDTH=1280
HEIGHT=720
PORT=8080
DEVICE=/dev/video0
FILE="/var/www/html/video/snapshot-`date +"%Y-%m-%d_%H.%M.%S"`.mp4"
v4l2-ctl --device=$DEVICE --set-fmt-video=width=$WIDTH,height=$HEIGHT,pixelformat=1
v4l2-ctl --set-parm=$FPS
#cvlc v4l2://$DEVICE:chroma=h264:width=$WIDTH:height=$HEIGHT:fps=$FPS --sout '#standard{access=http,mux=ts,dst=0.0.0.0:$PORT,name=stream,mime=video/ts}' -vv
#cvlc v4l2://$DEVICE:chroma=h264:width=$WIDTH:height=$HEIGHT:fps=$FPS --sout '#transcode{vcodec=h264,vb=6000,acodec=mp4a,aenc=fdkaac,ab=256}:file{dst=$FILE}' -vv --run-time=1800 --stop-time=1800 vlc://quit


#cvlc v4l2://$DEVICE:chroma=h264:width=$WIDTH:height=$HEIGHT:fps=$FPS --sout '#transcode{vcodec=h264,vb=6000,acodec=mp4a,aenc=fdkaac,ab=256}:standard{access=file,mux=ts,dst=$FILE}' -vv --run-time=1800 --stop-time=1800 vlc://quit
#cvlc v4l2://$DEVICE:chroma=h264:width=$WIDTH:height=$HEIGHT:fps=$FPS --sout file/mp4:video.mp4 -vv

cvlc v4l2://$DEVICE:chroma=h264:width=$WIDTH:height=$HEIGHT:fps=$FPS --sout file/mp4:$FILE -vv --run-time=1800 --stop-time=1800 vlc://quit

