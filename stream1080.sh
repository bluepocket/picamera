#! /bin/bash
FPS=30
WIDTH=1920
HEIGHT=1080
PORT=8080
DEVICE=/dev/video0
v4l2-ctl --device=$DEVICE --set-fmt-video=width=$WIDTH,height=$HEIGHT,pixelformat=1
v4l2-ctl --set-parm=$FPS
cvlc v4l2://$DEVICE:chroma=h264:width=$WIDTH:height=$HEIGHT:fps=$FPS --sout '#standard{access=http,mux=ts,dst=0.0.0.0:$PORT,name=stream,mime=video/ts}' -vv

