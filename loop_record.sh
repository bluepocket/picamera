#!/bin/bash
set -x
DATE=$(date +"%Y-%m-%d_%H%M")
FILEDIR=$1
FILE=$DATE.jpg
if [[ "$FILEDIR" != "" ]]; then
	FILE="$FILEDIR"/"$FILE"
fi
shift;
NOT_DONE=true
TRY=0
while [[ "$NOT_DONE" == "true" ]]; do
	RESULT=$(save720.sh) 
#	if [[ "$RESULT" == "0" ]]; then
#		rm "$FILEDIR/$DATE.jpg"
#		TRY=$((TRY+1))
#		if [[ $TRY > 5 ]]; then
#			echo "Failed 5 times, exiting with error"
#			NOT_DONE=false
#			exit 1
#		else
#			echo "Failed, going to try again."
#		fi
#	else
#		NOT_DONE=false
#	fi

	# cleanup
	ls --reverse -1 /var/www/html/video/*.mp4 | tail -n +11 | xargs rm -v
done

